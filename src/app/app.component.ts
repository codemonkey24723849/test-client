import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { BooksPage } from '../pages/books/books';
import { NgRedux } from '@angular-redux/store';
import { IAppState } from '../shared/redux/state/app-state.interface';
import { configureStore } from '../shared/redux/store/index';
import { AuthService } from '../shared/services/auth.service';

@Component({
  template: `<ion-nav [root]="rootPage"></ion-nav>`
})
export class MyApp {
  rootPage:any = BooksPage;

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private redux: NgRedux<IAppState>,
              private authService: AuthService) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  public ngOnInit() {
    configureStore(this.redux);
    this.authService.me();
  }
}
