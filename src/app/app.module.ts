import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { BooksPage } from '../pages/books/books';
import { BookDetailPage } from '../pages/book-detail/book-detail';
import { UserProfilePopover } from '../shared/popovers/user-profile-popover';
import { SignInTabsContainer } from '../shared/modals/sign-in/tabs-container';
import { SignInTab } from '../shared/modals/sign-in/sign-in-tab';
import { SignUpTab } from '../shared/modals/sign-in/sign-up-tab';
import { PROVIDER_SERVICE } from '../shared/provider/constants';
import { SailsProviderService } from '../shared/provider/sails-provider.service';
import { APP_CONFIG } from '../shared/config/config.constants';
import { LOCAL_APP_CONFIG } from '../config/config.local';
import { ConfigModule } from '../shared/config/config.module';
import { NgReduxModule } from '@angular-redux/store';
import { AuthService } from '../shared/services/auth.service';
import { BooksService } from '../shared/services/books.service';
import { CustomHeader } from './custom-header.component';

@NgModule({
  declarations: [
    MyApp,
    BooksPage,
    BookDetailPage,
    UserProfilePopover,
    SignInTabsContainer,
    SignInTab,
    SignUpTab,
    CustomHeader
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    ConfigModule,
    NgReduxModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BooksPage,
    BookDetailPage,
    UserProfilePopover,
    SignInTabsContainer,
    SignInTab,
    SignUpTab
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: PROVIDER_SERVICE, useClass: SailsProviderService },
    { provide: APP_CONFIG, useValue: LOCAL_APP_CONFIG },
    AuthService,
    BooksService
  ]
})
export class AppModule {
  constructor() {

  }
}
