import { Component, Input } from '@angular/core';
import { IReduxUserEntities } from '../shared/redux/interfaces/user-entities.interface';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';
import { IReduxUser } from '../shared/redux/interfaces/user.interface';
import { UserProfilePopover } from '../shared/popovers/user-profile-popover';
import { SignInTabsContainer } from '../shared/modals/sign-in/tabs-container';
import { PopoverController, ModalController } from 'ionic-angular';

@Component({
  selector: 'custom-header',
  template: `
    <ion-navbar>
      <ion-title>{{title}}</ion-title>        
      <ion-buttons end>
        <button *ngIf="user" ion-button icon-only color="royal" (click)="presentPopover($event)">
          <ion-icon name="person"></ion-icon>{{user.username}}
        </button>
        <button *ngIf="!user" ion-button icon-only color="royal" (click)="presentSignInModal($event)">
          Войти
        </button>
      </ion-buttons>
    </ion-navbar>
  `
})
export class CustomHeader {
  public user: IReduxUser = null;

  @Input() public title: string;

  @select() private userId$: Observable<number>;
  @select(['entities', 'users']) private userEntities$: Observable<IReduxUserEntities>;

  private userId: number = 0;
  private userEntities: IReduxUserEntities = {};

  constructor(public popoverCtrl: PopoverController,
              public modalCtrl: ModalController) {

  }

  public ngOnInit() {
    this.userId$.subscribe((userId: number) => {
      this.userId = userId;
      this.initUser();
    });
    this.userEntities$.subscribe((users: IReduxUserEntities) => {
      this.userEntities = users;
      this.initUser();
    });
  }

  presentPopover(ev) {
    let popover = this.popoverCtrl.create(UserProfilePopover);
    popover.present({
      ev
    });
  }

  presentSignInModal() {
    let modal = this.modalCtrl.create(SignInTabsContainer);
    modal.present();
  }

  private initUser() {
    this.user = null;
    if (this.userId && this.userEntities) {
      if (this.userEntities[this.userId] && this.userEntities[this.userId].username !== 'guest') {
        this.user = this.userEntities[this.userId];
      }
    }
  }
}
