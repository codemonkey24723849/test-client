import { NavParams, AlertController } from 'ionic-angular';
import { Component } from '@angular/core';
import { select } from "@angular-redux/store";
import { Observable } from "rxjs";
import { IReduxUserEntities } from "../../shared/redux/interfaces/user-entities.interface";
import { IReduxUser } from "../../shared/redux/interfaces/user.interface";
import { BooksService } from "../../shared/services/books.service";
import { IReduxBookEntities } from "../../shared/redux/interfaces/book-entities.interface";
import { IReduxAuthorEntities } from '../../shared/redux/interfaces/author-entities.interface';

@Component({
  template: `
    <ion-header>
      <ion-header>
          <custom-header [title]="item.name"></custom-header>
        </ion-header>
    </ion-header>
    
    <ion-content padding>
      <img *ngIf="item.smallThumbUrl" [src]="item.smallThumbUrl" alt="" />
      <p *ngIf="item.author && authorEntities[item.author]">Автор: {{authorEntities[item.author].firstName}} {{authorEntities[item.author].lastName}}</p>
      <p>{{item.description}}</p>
      <button ion-button color="secondary" outline [hidden]="!(!item.borrowedBy && user)" (click)="borrowBook()">Взять книгу</button>
      <button ion-button color="secondary" outline [hidden]="!(item.borrowedBy && user && user.id == item.borrowedBy)" (click)="returnBook()">Вернуть книгу</button>
      <p *ngIf="item.borrowedBy && user && userEntities[item.borrowedBy] && user.id != item.borrowedBy">Книга находится у пользователя {{userEntities[item.borrowedBy].username}}</p>
      <p *ngIf="item.borrowedBy && !user && userEntities[item.borrowedBy]">Книга находится у пользователя {{userEntities[item.borrowedBy].username}}</p>
      <p *ngIf="!item.borrowedBy && !user">Для того, чтобы взять книгу, вам необходимо <a (click)="presentSignInModal()">войти</a></p>
    </ion-content>`,
})
export class BookDetailPage {
  public itemId = null;
  public item = null;

  @select() private userId$: Observable<number>;
  @select(['entities', 'users']) private userEntities$: Observable<IReduxUserEntities>;

  private userId: number = 0;
  private userEntities: IReduxUserEntities = {};

  private user: IReduxUser = null;

  @select(['entities', 'books']) private bookEntities$: Observable<IReduxBookEntities>;

  @select(['entities', 'authors']) private authorEntities$: Observable<IReduxAuthorEntities>;
  private authorEntities: IReduxAuthorEntities = {};

  constructor(params: NavParams,
              public booksService: BooksService,
              public alertCtrl: AlertController) {
    this.itemId = params.data.itemId;
  }

  public ngOnInit() {
    this.userId$.subscribe((userId: number) => {
      this.userId = userId;
      this.initUser();
    });
    this.userEntities$.subscribe((users: IReduxUserEntities) => {
      this.userEntities = users;
      this.initUser();
    });
    this.bookEntities$.subscribe((books: IReduxBookEntities) => {
      if (books[this.itemId]) {
        this.item = books[this.itemId];
      }
    });
    this.authorEntities$.subscribe((authorEntities: IReduxAuthorEntities) => {
      this.authorEntities = authorEntities;
    });
  }

  public borrowBook() {
    this.booksService.borrowBook(this.item).then(() => {
      let alert = this.alertCtrl.create({
        title: 'Книга ' + this.item.name,
        subTitle: 'Вы успешно взяли книгу!',
        buttons: ['OK']
      });
      alert.present();
    }).catch(err => {
      let alert = this.alertCtrl.create({
        title: 'Книга ' + this.item.name,
        subTitle: 'Произошла ошибка, обратитесь к администратору!',
        buttons: ['OK']
      });
      alert.present();
    });
  }

  public returnBook() {
    this.booksService.returnBook(this.item).then(() => {
      let alert = this.alertCtrl.create({
        title: 'Книга ' + this.item.name,
        subTitle: 'Вы успешно вернули книгу!',
        buttons: ['OK']
      });
      alert.present();
    }).catch(err => {
      let alert = this.alertCtrl.create({
        title: 'Книга ' + this.item.name,
        subTitle: 'Произошла ошибка, обратитесь к администратору!',
        buttons: ['OK']
      });
      alert.present();
    });
  }

  private initUser() {
    this.user = null;
    if (this.userId && this.userEntities) {
      if (this.userEntities[this.userId] && this.userEntities[this.userId].username !== 'guest') {
        this.user = this.userEntities[this.userId];
      }
    }
  }
}
