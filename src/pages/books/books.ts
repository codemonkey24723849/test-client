import { Component } from '@angular/core';
import {
  NavController
} from 'ionic-angular'
import { BookDetailPage } from '../book-detail/book-detail';
import { Observable } from 'rxjs';
import { select } from '@angular-redux/store';
import { IReduxUserEntities } from '../../shared/redux/interfaces/user-entities.interface';
import { BooksService } from '../../shared/services/books.service';
import { IReduxBookEntities } from '../../shared/redux/interfaces/book-entities.interface';
import { IReduxBook } from '../../shared/redux/interfaces/book.interface';
import { IReduxUser } from '../../shared/redux/interfaces/user.interface';

@Component({
  selector: 'page-home',
  template: `
    <ion-header>
      <custom-header title="Книги"></custom-header>
    </ion-header>
    
    <ion-content>
      <ion-spinner [hidden]="!(actionInProcess && books.length == 0)"></ion-spinner>
      <ion-list [hidden]="actionInProcess && books.length == 0">
        <button ion-item *ngFor="let item of books" (click)="openNavDetailsPage(item)" icon-left>
          <ion-avatar item-left *ngIf="item.smallThumbUrl">
            <img [src]="item.smallThumbUrl">
          </ion-avatar>
		      <h2 *ngIf="!item.borrowedBy" class="free-book-item">{{item.name}}</h2>
		      <h2 *ngIf="item.borrowedBy && user && (item.borrowedBy == user.id)" class="borrowed-book-item">{{item.name}} (Книга находится у вас)</h2>
		      <h2 *ngIf="item.borrowedBy && user && (item.borrowedBy != user.id) && userEntities[item.borrowedBy]" class="borrowed-book-item">{{item.name}}  (Книга находится у пользователя {{userEntities[item.borrowedBy].username}})</h2>
		      <h2 *ngIf="item.borrowedBy && !user && userEntities[item.borrowedBy]" class="borrowed-book-item">{{item.name}} (Книга находится у пользователя {{userEntities[item.borrowedBy].username}})</h2>
		      <h3>{{item.description}}</h3>          
        </button>
      </ion-list>
    </ion-content>
  `
})
export class BooksPage {
  @select() private userId$: Observable<number>;
  @select(['entities', 'users']) private userEntities$: Observable<IReduxUserEntities>;

  @select(['entities', 'books']) private bookEntities$: Observable<IReduxBookEntities>;
  private bookEntities: IReduxBookEntities = {};

  @select(['layouts', 'booksList', 'items']) private bookIds$: Observable<number[]>;
  private bookIds: number[] = [];

  @select(['layouts', 'booksList', 'actionInProcess']) private actionInProcess$: Observable<boolean>
  public actionInProcess: boolean = false;

  private userId: number = 0;
  private userEntities: IReduxUserEntities = {};

  private user: IReduxUser = null;

  private books: IReduxBook[] = [];

  private spinnerTimeoutId: number = 0;

  constructor(public nav: NavController,
              public booksService: BooksService) {

  }

  public ngOnInit() {
    this.userId$.subscribe((userId: number) => {
      this.userId = userId;
      this.initUser();
    });
    this.userEntities$.subscribe((users: IReduxUserEntities) => {
      this.userEntities = users;
      this.initUser();
    });
    this.bookEntities$.subscribe((books: IReduxBookEntities) => {
      this.bookEntities = books;
      this.initBooks();
    });
    this.bookIds$.subscribe((bookIds: number[]) => {
      this.bookIds = bookIds;
      this.initBooks();
    });
    this.actionInProcess$.subscribe((actionInProcess: boolean) => {
      if (this.spinnerTimeoutId) {
        window.clearTimeout(this.spinnerTimeoutId);
      }
      if (actionInProcess) {
        this.spinnerTimeoutId = window.setTimeout(() => {
          this.actionInProcess = true;
        }, 1000);
      } else {
        this.actionInProcess = false;
      }
    });

    this.booksService.list();
    this.booksService.watchBooks();
  }

  openNavDetailsPage(item) {
    this.nav.push(BookDetailPage, { itemId: item.id });
  }

  private initBooks() {
    this.books = [];
    for (let bookId of this.bookIds) {
      if (this.bookEntities[bookId]) {
        this.books.push(this.bookEntities[bookId]);
      }
    }
  }

  private initUser() {
    this.user = null;
    if (this.userId && this.userEntities) {
      if (this.userEntities[this.userId] && this.userEntities[this.userId].username !== 'guest') {
        this.user = this.userEntities[this.userId];
      }
    }
  }
}
