import { IAppConfig } from '../shared/config/config.interface';

export const LOCAL_APP_CONFIG: IAppConfig = {
    api: {
        url: 'http://office.freshdoc.ru:40021'
    }
};
