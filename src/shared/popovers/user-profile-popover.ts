import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { AuthService } from '../services/auth.service';

@Component({
  template: `<ion-list>
      <button ion-item (click)="close()">Выйти</button>
    </ion-list>`,
})
export class UserProfilePopover {
  constructor(public viewCtrl: ViewController, private authService: AuthService) {}

  close() {
    this.viewCtrl.dismiss();
    this.authService.logout();
  }
}
