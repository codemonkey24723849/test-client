import { Injectable, Inject } from '@angular/core';
import { PROVIDER_SERVICE } from '../provider/constants';
import { IProviderService } from '../provider/provider.interface';
import { NgRedux } from '@angular-redux/store';
import { IAppState } from '../redux/state/app-state.interface';
import {
  INIT_USER_REQUEST, INIT_USER_SUCCESS,
  INIT_USER_ERROR, SIGN_IN_REQUEST, SIGN_IN_SUCCESS, SIGN_IN_ERROR, SIGN_OUT_REQUEST,
  SIGN_OUT_SUCCESS, SIGN_OUT_ERROR, SIGN_UP_REQUEST, SIGN_UP_SUCCESS, SIGN_UP_ERROR
} from '../redux/constants/user.constants';
import { IApiUser } from '../api-interfaces/user.interface';
import { userSchema } from '../normalizr/schemas/user.schema';
import { normalize } from 'normalizr';

@Injectable()
export class AuthService {
  constructor(@Inject(PROVIDER_SERVICE) private providerService: IProviderService,
              private redux: NgRedux<IAppState>) {

  }

  me(): Promise<any> {
    this.redux.dispatch({
      type: INIT_USER_REQUEST
    });
    return this.providerService.me()
      .then(response => this.getUserDataFromResponse(response))
      .then(user => normalize(user, userSchema))
      .then(user => {
        this.redux.dispatch({
          type: INIT_USER_SUCCESS,
          payload: user
        });
        return user;
      }).catch(err => {
        this.redux.dispatch({
          type: INIT_USER_ERROR,
          payload: err
        });
        throw err;
      });
  }

  login(email, password): Promise<any> {
    this.redux.dispatch({
      type: SIGN_IN_REQUEST
    });
    return this.providerService.login(email, password)
      .then(response => this.getUserDataFromResponse(response))
      .then(user => normalize(user, userSchema))
      .then(user => {
        this.redux.dispatch({
          type: SIGN_IN_SUCCESS,
          payload: user
        });
        return user;
      }).catch(err => {
        this.redux.dispatch({
          type: SIGN_IN_ERROR,
          payload: err
        });
        throw err;
      });
  }

  logout(): Promise<any> {
    this.redux.dispatch({
      type: SIGN_OUT_REQUEST
    });
    return this.providerService.logout()
      .then((res) => {
        this.redux.dispatch({
          type: SIGN_OUT_SUCCESS
        });
      }).catch(err => {
        this.redux.dispatch({
          type: SIGN_OUT_ERROR,
          payload: err
        });
        throw err;
      });
  }

  register (email, password, options = {}) {
    this.redux.dispatch({
      type: SIGN_UP_REQUEST
    });
    return this.providerService.register(email, password, options)
      .then(response => this.getUserDataFromResponse(response))
      .then(user => normalize(user, userSchema))
      .then((res) => {
        this.redux.dispatch({
          type: SIGN_UP_SUCCESS,
          payload: res
        });
      }).catch(err => {
        this.redux.dispatch({
          type: SIGN_UP_ERROR,
          payload: err
        });
        throw err;
      });
  }

  getUserDataFromResponse(response: any): IApiUser {
    return {
      id: response.id,
      email: response.email,
      username: response.username,
      roles: response.roles
    };
  }
}
