import { Injectable, Inject } from '@angular/core';
import { PROVIDER_SERVICE } from '../provider/constants';
import { IProviderService } from '../provider/provider.interface';
import { NgRedux } from '@angular-redux/store';
import { IAppState } from '../redux/state/app-state.interface';
import { normalize, schema } from 'normalizr';
import { IApiBook } from '../api-interfaces/book.interface';
import {
  GET_BOOKS_REQUEST, GET_BOOKS_SUCCESS,
  GET_BOOKS_ERROR, BORROW_BOOK_REQUEST, BORROW_BOOK_SUCCESS, BORROW_BOOK_ERROR, RETURN_BOOK_REQUEST,
  RETURN_BOOK_SUCCESS, RETURN_BOOK_ERROR
} from '../redux/constants/book.constants';
import { bookSchema } from '../normalizr/schemas/book.schema';

@Injectable()
export class BooksService {
  constructor(@Inject(PROVIDER_SERVICE) private providerService: IProviderService,
              private redux: NgRedux<IAppState>) {

  }

  list() {
    this.redux.dispatch({
      type: GET_BOOKS_REQUEST
    });
    this.providerService.list('catalog/all-books', {
      populate: 'borrowedBy,author'
    })
      .then(res => res.map(this.getBookDataFromResponse))
      .then(res => normalize(res, new schema.Array(bookSchema)))
      .then(res => {
        this.redux.dispatch({
          type: GET_BOOKS_SUCCESS,
          payload: res
        });
      })
      .catch(err => {
        this.redux.dispatch({
          type: GET_BOOKS_ERROR,
          payload: err
        });
        throw err;
      });
  }

  borrowBook(book) {
    this.redux.dispatch({
      type: BORROW_BOOK_REQUEST,
      payload: {
        id: book.id
      }
    });
    return this.providerService.execute('user', 'borrow-book', {
      id: book.id
    })
      .then(res => this.getBookDataFromResponse(res))
      .then(res => normalize(res, bookSchema))
      .then(res => {
        this.redux.dispatch({
          type: BORROW_BOOK_SUCCESS,
          payload: res
        });
      })
      .catch(err => {
        this.redux.dispatch({
          type: BORROW_BOOK_ERROR,
          payload: err
        });
        throw err;
      });
  }

  returnBook(book) {
    this.redux.dispatch({
      type: RETURN_BOOK_REQUEST,
      payload: {
        id: book.id
      }
    });
    return this.providerService.execute('user', 'return-book', {
      id: book.id
    })
      .then(res => this.getBookDataFromResponse(res))
      .then(res => normalize(res, bookSchema))
      .then(res => {
        this.redux.dispatch({
          type: RETURN_BOOK_SUCCESS,
          payload: res
        });
      })
      .catch(err => {
        this.redux.dispatch({
          type: RETURN_BOOK_ERROR,
          payload: err
        });
        throw err;
      });
  }

  watchBooks() {
    this.providerService.listen('book', (event) => {
      let res;
      switch (event.type) {
        case 'book-borrowed':
          res = this.getBookDataFromResponse(event.data);
          res =  normalize(res, bookSchema);
          this.redux.dispatch({
            type: BORROW_BOOK_SUCCESS,
            payload: res
          });
          break;
        case 'book-returned':
          res = this.getBookDataFromResponse(event.data);
          res =  normalize(res, bookSchema);
          this.redux.dispatch({
            type: BORROW_BOOK_SUCCESS,
            payload: res
          });
          break;
      }
    })
      .catch(err => {
        console.warn(err);
      });
  }

  getBookDataFromResponse(response: any): IApiBook {
    return {
      id: response.id,
      name: response.name,
      description: response.description,
      smallThumbUrl: response.smallThumbUrl,
      mediumThumbUrl: response.mediumThumbUrl,
      author: response.author,
      borrowedBy: response.borrowedBy
    };
  }
}
