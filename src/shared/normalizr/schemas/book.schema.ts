import { schema } from 'normalizr';
import { userSchema } from './user.schema';
import { authorSchema } from './author.schema';

export const bookSchema = new schema.Entity('books', {
  author: authorSchema,
  borrowedBy: userSchema,
});
