import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { CustomValidators } from '../../validators/index';
import { AuthService } from '../../services/auth.service';
import { ToastController } from 'ionic-angular';

@Component({
  template: `<ion-header>
  <ion-toolbar>
    <ion-title>
      Вход
    </ion-title>
    <ion-buttons start>
      <button ion-button (click)="dismiss()">
        <span ion-text color="primary">Cancel</span>
      </button>
    </ion-buttons>
  </ion-toolbar>
</ion-header>

<ion-content>
  <form [formGroup]="group" (ngSubmit)="signIn()" novalidate>
    <ion-list>
      <ion-item>
        <ion-label fixed>Username</ion-label>
        <ion-input [formControl]="email" autofocus type="text"></ion-input>
      </ion-item>
    
      <ion-item>
        <ion-label fixed>Password</ion-label>
        <ion-input [formControl]="password" type="password"></ion-input>
      </ion-item>
    
    </ion-list>
    
    <div padding>
      <button ion-button [disabled]="!group.valid || actionInProcess">Войти</button>
    </div>
  </form>
</ion-content>`,
})
export class SignInTab {
  email: FormControl;
  password: FormControl;
  group: FormGroup;

  // @todo. костыль. Если вызывать viewCtrl.dismiss() из этого класса, то возникнет ошибка
  public static dismissCallback: any;

  // private actionInProcess: boolean = false;

  constructor(private formBuilder: FormBuilder, private authService: AuthService,
              public toastCtrl: ToastController) {}

  dismiss() {
    SignInTab.dismissCallback();
  }

  ngOnInit() {
    this.email = new FormControl('', Validators.compose([Validators.required, CustomValidators.emailFormat]));
    this.password = new FormControl('', Validators.compose([Validators.required, Validators.minLength(8)]));
    this.group = this.formBuilder.group({
      email: this.email,
      password: this.password
    });
  }

  signIn() {
    let email = this.group.value.email;
    let password = this.group.value.password;

    this.authService.login(email, password).then(res => {
      this.dismiss();
    }).catch(err => {
      let toast = this.toastCtrl.create({
        message: 'Неверный логин или пароль',
        duration: 3000
      });
      toast.present();
    });
  }
}
