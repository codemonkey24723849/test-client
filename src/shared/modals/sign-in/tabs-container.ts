import { Component } from '@angular/core';
import { SignInTab } from './sign-in-tab';
import { SignUpTab } from './sign-up-tab';
import { ViewController } from 'ionic-angular';

@Component({
  template: `
<ion-tabs>
  <ion-tab tabTitle="Вход" [root]="signInTab"></ion-tab>
  <ion-tab tabTitle="Регистрация" [root]="signUpTab"></ion-tab>
</ion-tabs>
`,
})
export class SignInTabsContainer {
  signInTab: any;
  signUpTab: any;

  constructor(private viewCtrl: ViewController) {
    this.signInTab = SignInTab;
    this.signUpTab = SignUpTab;

    SignInTab.dismissCallback = this.dismiss.bind(this);
    SignUpTab.dismissCallback = this.dismiss.bind(this);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
