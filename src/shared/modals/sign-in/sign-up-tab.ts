import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { CustomValidators } from '../../validators/index';
import { AuthService } from '../../services/auth.service';
import { ToastController } from 'ionic-angular';

@Component({
  template: `<ion-header>
  <ion-toolbar>
    <ion-title>
      Регистрация
    </ion-title>
    <ion-buttons start>
      <button ion-button (click)="dismiss()">
        <span ion-text color="primary">Cancel</span>
      </button>
    </ion-buttons>
  </ion-toolbar>
</ion-header>

<ion-content>
  <form [formGroup]="group" (ngSubmit)="signUp()" novalidate>
    <ion-list>
      <ion-item>
        <ion-label fixed>Username</ion-label>
        <ion-input [formControl]="email" autofocus type="text"></ion-input>
      </ion-item>
    
      <ion-item>
        <ion-label fixed>Password</ion-label>
        <ion-input [formControl]="password" type="password"></ion-input>
      </ion-item>
    
    </ion-list>
    
    <div padding>
      <button ion-button [disabled]="!group.valid || actionInProcess">Зарегистрироваться</button>
    </div>
  </form>
</ion-content>`,
})
export class SignUpTab {
  email: FormControl;
  password: FormControl;
  group: FormGroup;

  // @todo. костыль. Если вызывать viewCtrl.dismiss() из этого класса, то возникнет ошибка
  public static dismissCallback: any;

  // private actionInProcess: boolean = false;

  constructor(private formBuilder: FormBuilder, private authService: AuthService,
              public toastCtrl: ToastController) {}

  dismiss() {
    SignUpTab.dismissCallback();
  }

  ngOnInit() {
    this.email = new FormControl('', Validators.compose([Validators.required, CustomValidators.emailFormat]));
    this.password = new FormControl('', Validators.compose([Validators.required, Validators.minLength(8)]));
    this.group = this.formBuilder.group({
      email: this.email,
      password: this.password
    });
  }

  signUp() {
    let email = this.group.value.email;
    let password = this.group.value.password;

    this.authService.register(email, password).then(res => {
      this.dismiss();
    }).catch(err => {
      // @todo. Сервер пока неверно возвращает текст ошибки, поэтому нет возможности нормально
      // определять, какая ошибка произошла на сервере. Но чаще всего это повторяющийся емэйл
      let toast = this.toastCtrl.create({
        message: 'Пользователь с таким емэйлом уже существует',
        duration: 3000
      });
      toast.present();
    });
  }
}
