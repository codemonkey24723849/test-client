import { IReduxAuthorEntities } from '../interfaces/author-entities.interface';
import { IReduxBookEntities } from '../interfaces/book-entities.interface';
import { IReduxBooksList } from '../interfaces/books-list.interface';
import { IReduxUserEntities } from '../interfaces/user-entities.interface';

export interface IAppState {
  userId: number;
  layouts: {
    booksList: IReduxBooksList
  };
  entities: {
    books: IReduxBookEntities,
    authors: IReduxAuthorEntities,
    users: IReduxUserEntities,
  };
};
