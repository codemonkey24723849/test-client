import { IAppState } from './app-state.interface';

export let initialAppState: IAppState = {
  userId: 0,
  layouts: {
    booksList: {
      items: [],
      loadingInProcess: false
    }
  },
  entities: {
    books: {},
    authors: {},
    users: {}
  }
};
