import { IReduxAuthor } from './author.interface';

export interface IReduxAuthorEntities {
    [id: number]: IReduxAuthor;
};
