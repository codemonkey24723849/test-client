import { IReduxUser } from './user.interface';

export interface IReduxUserEntities {
    [id: number]: IReduxUser;
};
