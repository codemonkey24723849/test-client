export interface IReduxBooksList {
    items: number[];
    loadingInProcess: boolean;
};
