export interface IReduxUser {
    id: number;
    email: string;
    username: string;
    roles: string[];
};
