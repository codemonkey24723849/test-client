export interface IReduxBook {
    id: string;
    name: string;
    description: string;
    borrowedBy: string;
    author: string;
};
