import { IReduxBook } from './book.interface';

export interface IReduxBookEntities {
    [id: number]: IReduxBook;
};
