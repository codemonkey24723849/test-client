import { NgRedux } from '@angular-redux/store';

import { configureDevStore } from './configure-store.dev';
// import { configureProdStore } from './configure-store.prod';
import { IAppState } from '../state/app-state.interface';

export function configureStore(ngRedux: NgRedux<IAppState>) {
    // if (process.env.ENV === 'production') {
    //     configureProdStore(ngRedux);
    // } else {
        configureDevStore(ngRedux);
    // }
}
