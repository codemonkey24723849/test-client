import { NgRedux } from '@angular-redux/store';
import { IAppState, initialAppState } from '../state';

import { rootReducer } from '../reducers';

export function configureProdStore(ngRedux: NgRedux<IAppState>) {
    ngRedux.configureStore(rootReducer, initialAppState);
}
