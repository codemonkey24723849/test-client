import { createLogger } from 'redux-logger';
import { NgRedux } from '@angular-redux/store';
import { IAppState, initialAppState } from '../state';

import { rootReducer } from '../reducers';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';

const stateKeeper = store => next => action => {
  let result = next(action);
  localStorage.setItem('state', JSON.stringify(store.getState()));
  return result;
}

export function configureDevStore(ngRedux: NgRedux<IAppState>) {
  let enhancers: Array<any> = [];
  if (window.devToolsExtension) {
    enhancers = [...enhancers, window.devToolsExtension()];
  }

  let stateStr = localStorage.getItem('state');
  let state: IAppState;
  if (stateStr) {
    state = JSON.parse(stateStr);
  } else {
    state = initialAppState;
  }

  ngRedux.configureStore(rootReducer, state, [
    reduxImmutableStateInvariant(),
    stateKeeper,
    createLogger()
  ], enhancers);
}
