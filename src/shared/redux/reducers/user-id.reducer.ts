import {
  INIT_USER_SUCCESS, SIGN_OUT_SUCCESS, SIGN_IN_SUCCESS,
  SIGN_UP_SUCCESS
} from '../constants/user.constants';

export function userId(state: number = 0, action: any): number {
  switch (action.type) {
    case INIT_USER_SUCCESS:
      return action.payload.result;
    case SIGN_IN_SUCCESS:
      return action.payload.result;
    case SIGN_UP_SUCCESS:
      return action.payload.result;
    case SIGN_OUT_SUCCESS:
      return 0;
    default:
      return state;
  }
}
