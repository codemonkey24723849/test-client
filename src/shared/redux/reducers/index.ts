import { combineReducers } from 'redux';

import { IAppState } from '../state';

import { userId } from './user-id.reducer';
import { layouts } from './layouts.reducer';
import { entities } from './entities.reducer';

export let rootReducer = combineReducers<IAppState>({
    userId,
    layouts,
    entities
});

