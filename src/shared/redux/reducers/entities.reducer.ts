import { combineReducers } from 'redux';
import { userEntities } from './user-entities.reducer';
import { bookEntities } from './book-entities.reducer';
import { authorEntities } from './author-entities.reducer';

export let entities = combineReducers<any>({
    books: bookEntities,
    authors: authorEntities,
    users: userEntities
});
