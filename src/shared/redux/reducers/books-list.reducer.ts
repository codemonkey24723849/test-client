import { IReduxBooksList } from '../interfaces/books-list.interface';
import { GET_BOOKS_SUCCESS, GET_BOOKS_REQUEST } from '../constants/book.constants';

export function booksList(state: IReduxBooksList = null, action: any) {
  switch (action.type) {
    case GET_BOOKS_REQUEST:
      return {
        ...state,
        actionInProcess: true
      };
    case GET_BOOKS_SUCCESS:
      return {
        items: action.payload.result,
        actionInProcess: false
      };
    default:
      return state;
  }
}
