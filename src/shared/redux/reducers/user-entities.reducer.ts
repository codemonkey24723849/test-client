import { IReduxUserEntities } from '../interfaces/user-entities.interface';
import { INIT_USER_SUCCESS, SIGN_IN_SUCCESS, SIGN_UP_SUCCESS } from '../constants/user.constants';
import {
  GET_BOOKS_SUCCESS, BORROW_BOOK_SUCCESS
} from '../constants/book.constants';

export function userEntities(state: IReduxUserEntities = null, action: any): IReduxUserEntities {
  switch (action.type) {
    case INIT_USER_SUCCESS:
      return {
        ...state,
        ...action.payload.entities.users
      };
    case SIGN_IN_SUCCESS:
      return {
        ...state,
        ...action.payload.entities.users
      };
    case SIGN_UP_SUCCESS:
      return {
        ...state,
        ...action.payload.entities.users
      };
    case GET_BOOKS_SUCCESS:
      return {
        ...state,
        ...action.payload.entities.users
      };
    case BORROW_BOOK_SUCCESS:
      if (action.payload.entities.users) {
        return {
          ...state,
          ...action.payload.entities.users
        };
      }
    default:
      return state;
  }
}
