import { combineReducers } from 'redux';

import { booksList } from './books-list.reducer';

export let layouts = combineReducers<any>({
    booksList
});
