import { IReduxAuthorEntities } from '../interfaces/author-entities.interface';
import { GET_BOOKS_SUCCESS } from '../constants/book.constants';

export function authorEntities(state: IReduxAuthorEntities = null, action: any): IReduxAuthorEntities {
  switch (action.type) {
    case GET_BOOKS_SUCCESS:
      return {
        ...state,
        ...action.payload.entities.authors
      };
    default:
      return state;
  }
}
