import { IReduxBookEntities } from '../interfaces/book-entities.interface';
import {GET_BOOKS_SUCCESS, BORROW_BOOK_SUCCESS, RETURN_BOOK_SUCCESS} from '../constants/book.constants';

export function bookEntities(state: IReduxBookEntities = null, action: any): IReduxBookEntities {
  switch (action.type) {
    case GET_BOOKS_SUCCESS:
      return {
        ...state,
        ...action.payload.entities.books
      };
    case BORROW_BOOK_SUCCESS:
      return {
        ...state,
        ...action.payload.entities.books
      };
    case RETURN_BOOK_SUCCESS:
      return {
        ...state,
        ...action.payload.entities.books
      };
    default:
      return state;
  }
}
