// Получение списка книг (begin)
export const GET_BOOKS_REQUEST = 'GET_BOOKS_REQUEST';
export const GET_BOOKS_SUCCESS = 'GET_BOOKS_SUCCESS';
export const GET_BOOKS_ERROR = 'GET_BOOKS_ERROR';
// Получение списка книг (end)

// Заимствование книги (begin)
export const BORROW_BOOK_REQUEST = 'BORROW_BOOK_REQUEST';
export const BORROW_BOOK_SUCCESS = 'BORROW_BOOK_SUCCESS';
export const BORROW_BOOK_ERROR = 'BORROW_BOOK_ERROR';
// Заимствование книги (end)

// Возврат книги (begin)
export const RETURN_BOOK_REQUEST = 'RETURN_BOOK_REQUEST';
export const RETURN_BOOK_SUCCESS = 'RETURN_BOOK_SUCCESS';
export const RETURN_BOOK_ERROR = 'RETURN_BOOK_ERROR';
// Возврат книги (end)
