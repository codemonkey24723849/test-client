import { FormControl } from '@angular/forms';

export class CustomValidators {
    static emailFormat(c: FormControl): { [s: string]: boolean } {
        let pattern: RegExp = /\S+@\S+\.\S+/;
        return pattern.test(c.value) ? null : {'emailFormat': true};
    }
}
