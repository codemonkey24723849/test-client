import { Injectable } from '@angular/core';
import { IProviderService } from './provider.interface';
import { Config } from '../config/config.service';

import socketIOClient from 'socket.io-client';
import sailsIOClient from 'sails.io.js';

class Subscription {
  public id: any;
  private socket;
  private provider;
  private model;
  private filter;
  private eventHandler;
  private destroyed;
  public request: any;

  /*
   * @param {SailsSocket} socket
   * @param {string} model
   * @param {string} filter JSON stringified object
   * @param {Function} cb
   */
  constructor (socket, provider, model, filter, cb) {
    /**
     * @type {string}
     * @public
     */
    this.id = '';

    /**
     * @type {SailsSocket}
     * @private
     */
    this.socket = socket;

    /**
     * @type {SailsProvider}
     * @private
     */
    this.provider = provider;

    /**
     * @type {string}
     * @private
     */
    this.model = model;

    /**
     * @type {string}
     * @private
     */
    this.filter = filter && (typeof filter === 'object') ? JSON.stringify(filter) : '';

    /**
     * @type {Function}
     * @private
     */
    this.eventHandler = this.handleEvent.bind(this, cb);

    /**
     * @type {boolean}
     * @private
     */
    this.destroyed = false;

    // Create subscription on server
    this.request = this.provider.request('POST', `/subscription/${this.model}`, {
      model: this.model,
      filter: this.filter
    })
      .then(id => {
        this.id = id;
        this.socket.on(this.model, this.eventHandler);
        return id;
      })
      .catch(err => {
        if (err.statusCode && err.statusCode === 403)
          this.destroy();
        throw err;
      });
  }

  /**
   * @param {Function} cb
   * @param {Object} event
   * @private
   */
  handleEvent (cb, event) {
    if (this.id && event.sids.indexOf(this.id) !== -1)
      cb({ id: event.id, type: event.type, data: event.data }); // TODO clone data
  }

  update () {
    if (this.destroyed)
      return;
    // this.provider.request('DELETE', `/subscription/${this.model}/${this.id}`);
    this.provider.request('POST', `/subscription/${this.model}`, {
      model: this.model,
      filter: this.filter
    })
      .then(id => { this.id = id; })
      .catch(err => {
        if (err.statusCode && err.statusCode === 403)
          this.destroy();
      });
  }

  destroy () {
    if (this.destroyed)
      return;
    this.destroyed = true;

    if (this.id) {
      this.socket.off(this.model, this.eventHandler);
      this.provider.request('DELETE', `/subscription/${this.model}/${this.id}`);
    }

    this.socket = null;
    this.provider = null;
    this.eventHandler = null;
  }

}

@Injectable()
export class SailsProviderService implements IProviderService {
  public socket: any;

  private baseUrl: any;
  private socketId: any;
  private subscriptions: any;
  private token: string;
  private services: any;

  constructor(private config: Config) {
    const io = sailsIOClient(socketIOClient);
    io.sails.autoConnect = false;

    /**
     * Базовый путь для ajax-запросов.
     * @type {string}
     * @private
     */
    this.baseUrl = config.get('api.url');
    let [, basePath, relativePath] = this.baseUrl.match(/^(https?\:\/\/[^\/?#]+)([\/?#]?.*)/i);

    let socketOptions: any = {
      useCORSRouteToGetCookie: relativePath + '/__getcookie'
    };
    if (this.baseUrl)
      socketOptions.url = basePath;

    /**
     * @type {Object}
     * @private
     */
    this.socket = io.sails.connect(socketOptions);

    /**
     * Сохраняем идентификатор сокета для определения переподключения.
     * @type {string}
     * @private
     */
    this.socketId = this.socket._raw ? this.socket._raw.id : '';

    /*// При восстановлении соединения, если сокет сам не переподключился,
    // подключаем его в приказном порядке.
    Offline.on('up', () => {
      this.socket._raw.disconnect();
      if (!this.socket.isConnected() && !this.socket.isConnecting())
        this.socket._raw.connect();
    });
    // Если айфон уснул и проснулся,
    // обязательно пересоздаем сокетное соединение.
    const timeout = 2000;
    let time = Date.now();
    setInterval(() => {
      const time2 = Date.now();
      if (time2 - time > timeout + 1300) {
        console.log('TEST');
        if (!this.socket.isConnected() && !this.socket.isConnecting())
          this.socket._raw.connect();
      }
      time = time2;
    }, timeout);
     */
    this.socket.on('disconnect', () => {
      this.socket._raw.connect();
    });

    // Обновление подписок делаем только ПОСЛЕ установки соединения!
    this.socket.on('connect', () => {
      if (!this.socketId) { // Первое подключение
        this.socketId = this.socket._raw.id;
      } else if (this.socketId !== this.socket._raw.id) { // Переподключение
        this.socketId = this.socket._raw.id;
        this.updateSubscriptions();
      }
    });

    /**
     * @type {string}
     * @private
     */
    this.token = localStorage.getItem('token') || '';

    /**
     * @type {Array.<Subscription>}
     * @private
     */
    this.subscriptions = [];

    /**
     * @type {Object.<string, string>}
     * @private
     */
    this.services = {};
  }

  /**
   * @param {string} accessToken
   */
  setToken (accessToken) {
    this.token = accessToken;
    localStorage.setItem('token', this.token);
    this.clearSubscriptions();
  }

  /**
   * @return {boolean}
   */
  isAuthenticated () {
    return !!this.token;
  }

  /**
   * @return {Promise.<User, JWRes>}
   * @public
   */
  me () {
    return this.request('GET', '/me');
  }

  /**
   * @param {string} identifier
   * @param {string} password
   * @return {Promise.<User, JWRes>}
   */
  login (identifier, password) {
    return new Promise((resolve, reject) => {
      this.socket.request({
        method: 'POST',
        url: '/login',
        data: {
          identifier: identifier,
          password: password
        }
      }, (data, jwres) => {
        if (jwres.statusCode !== 200) {
          jwres.url = '/auth/local';
          return reject(jwres);
        }
        this.setToken(data.accessToken);
        resolve(data.user);
      });
    });
  }

  /**
   * @return {Promise}
   */
  logout () {
    return new Promise((resolve, reject) => {
      this.socket.request({
        method: 'POST',
        url: '/logout'
      }, (data, jwres) => {
        if (jwres.statusCode !== 200) {
          jwres.url = '/logout';
          return reject(jwres);
        }
        this.token = '';
        localStorage.removeItem('token');
        this.clearSubscriptions();
        resolve();
      });
    });
  }

  /**
   * @param {string} email
   * @param {string} password
   * @param {Object=} [other={}]
   * @return {Promise.<User, JWRes>}
   */
  register (email, password, options = {}) {
    return new Promise((resolve, reject) => {
      this.socket.request({
        method: 'POST',
        url: '/register',
        data: Object.assign({
          email,
          password,
          username: email
        }, options)
      }, (data, jwres) => {
        if (jwres.statusCode !== 200) {
          jwres.url = '/register';
          return reject(jwres);
        }
        this.setToken(data.accessToken);
        resolve(data.user);
      });
    });
  }

  /**
   * @param {string} model Model's name
   * @param {Object=} params
   * @param {Object=} params.where ({project: '123', type: 'audio'}) - фильтрует по свойствам
   * @param {string=} params.sort ('createdAt ASC') - сортирует по полю
   * @param {number=} params.skip
   * @param {number=} params.limit
   * @param {string=} params.populate - вкладывает связанные ресурсы, например ('thumb,style')
   * @return {Promise.<Array.<Model>, JWRes>}
   */
  list (model, options: any = { where: null, sort: '', skip: 0, limit: 0, populate: '' }) {
    const params: any = {};
    if (options.where) params.where = JSON.stringify(options.where);
    if (options.sort) params.sort = encodeURIComponent(options.sort);
    if (options.skip) params.skip = +options.skip;
    if (options.limit) params.limit = +options.limit;
    if (options.populate) params.populate = encodeURIComponent(options.populate);
    if (options.page) params.page = JSON.stringify(options.page);

    let queryPairs = [];
    for (let key in params)
      queryPairs.push(key + '=' + params[key]);
    const query = queryPairs.join('&');
    const url = query ? `/${model}/?${query}` : `/${model}/`;

    return new Promise((resolve, reject) => {
      this.socket.request({
        method: 'GET',
        url: url,
        headers: { 'Authorization': 'Bearer ' + this.token }
      }, (data, jwres) => {
        if (jwres.statusCode !== 200) {
          jwres.url = url;
          return reject(jwres);
        }
        resolve(data);
      });
    });
  }

  /**
   * @param {string} model
   * @param {string} id
   * @param {Object=} options
   * @param {string=} options.populate
   * @return {Promise.<Model, JWRes>}
   */
  get (model, id, options = { populate: '' }) {
    return new Promise((resolve, reject) => {
      let query = '';
      if (options.populate)
        query = 'populate=' + encodeURIComponent(options.populate);
      const url = query ? `/${model}/${id}?${query}` : `/${model}/${id}`;
      this.socket.request({
        method: 'GET',
        url: url,
        headers: { 'Authorization': 'Bearer ' + this.token }
      }, (data, jwres) => {
        if (jwres.statusCode !== 200) {
          jwres.url = url;
          return reject(jwres);
        }
        resolve(data);
      });
    });
  }

  /**
   * @param {string} model
   * @param {Object|FormData} data
   * @return {Promise.<Model>, JWRes}
   */
  create (model, data) {
    return this.request('POST', `/${model}/`, data);
  }

  /**
   * @param {string} model
   * @param {string} id
   * @param {Object} data
   * @return {Promise.<Model, JWRes>}
   */
  update (model, id, data) {
    if (!id) {
      return Promise.reject({
        url: `/${model}/${id}`,
        statusCode: 400,
        body: null,
        headers: null
      });
    }
    return this.request('PUT', `/${model}/${id}`, data);
  }

  /**
   * @param {string} model
   * @param {string} id
   * @return {Promise.<Model, JWRes>}
   */
  remove (model, id) {
    if (!id) {
      return Promise.reject({
        url: `/${model}/${id}`,
        statusCode: 400,
        body: null,
        headers: null
      });
    }
    return new Promise((resolve, reject) => {
      this.socket.request({
        method: 'DELETE',
        url: `/${model}/${id}`,
        headers: { 'Authorization': 'Bearer ' + this.token }
      }, (data, jwres) => {
        if (jwres.statusCode !== 200) {
          jwres.url = `/${model}/${id}`;
          return reject(jwres);
        }
        resolve(data);
      });
    });
  }

  /**
   * @param {string} model
   * @param {string} id
   * @param {string} command
   * @param {Object=} [data=null]
   * @param {string=} [method='POST']
   * @return {Promise}
   * @public
   */
  executeOld (model, id, command, data = null, method = 'POST') {
    if (arguments.length === 4) {
      if (typeof data === 'string') {
        method = data;
        data = null;
      }
    }
    return this.request(method, `/${model}/${id}/${command}`, data);
  }

  /**
   * @param {string} model7
   * @param {string} command
   * @param {Object=} [data=null]
   * @param {string=} [method='POST']
   * @return {Promise}
   * @public
   */
  execute (model, command, data = null, method = 'POST') {
    if (arguments.length === 4) {
      if (typeof data === 'string') {
        method = data;
        data = null;
      }
    }
    return this.request(method, `/${model}/${command}`, data);
  }

  /**
   * @param {string} model
   * @param {string} id
   * @param {Object} data
   * @return {Promise.<Model, JWRes>}
   * @public
   * @final
   */
  request (method, url, data = {}) {
    let hasFiles = false;
    for (let key in data) {
      if (this._isFile(data[key])) {
        hasFiles = true;
        break;
      }
    }

    if (!hasFiles) {
      return new Promise((resolve, reject) => {
        this.socket.request({
          method: method.toUpperCase(),
          url: url,
          data: data,
          headers: { 'Authorization': 'Bearer ' + this.token }
        }, (data, jwres) => {
          if (jwres.statusCode !== 200 && jwres.statusCode !== 201) {
            jwres.url = url;
            return reject(jwres);
          }
          resolve(data);
        });
      });
    } else {
      data = this._reorderPayloadData(data); // Blame skipper!
      const form = new FormData();
      for (let key in data) {
        if (data[key] && typeof data[key] === 'object' && !(data[key] instanceof Blob))
          data[key] = JSON.stringify(data[key]);
        form.append(key, data[key]);
      }

      return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open(method, this.baseUrl + url);
        xhr.setRequestHeader('Authorization', 'Bearer ' + this.token);
        xhr.onreadystatechange = () => {
          if (xhr.readyState === 4) {
            if (xhr.status !== 200 && xhr.status !== 201) {
              reject({
                url: url,
                body: xhr.responseText ? JSON.parse(xhr.responseText) : {},
                statusCode: xhr.status,
                headers: {}
              });
            }
            resolve(xhr.responseText ? JSON.parse(xhr.responseText) : {});
          }
        };
        xhr.send(form);
      });
    }
  }

  /**
   * Переставляем поля объекта запроса таким образом, чтобы файлы и блобы были в конце.
   * Таково ограничение серверной части. skipper игнорирует скалярыне поля после файловых.
   * @param {Object} data
   * @return {Object}
   * @private
   */
  _reorderPayloadData (data) {
    const scalarKeys = [];
    const fileKeys = [];
    for (let key in data) {
      if (this._isFile(data[key]))
        fileKeys.push(key);
      else
        scalarKeys.push(key);
    }
    const ordered = {};
    const keys = [...scalarKeys, ...fileKeys];
    for (let key of keys)
      ordered[key] = data[key];
    return ordered;
  }

  /**
   * @param {*} value
   * @return {boolean}
   * @private
   */
  _isFile (value) {
    return value && typeof value === 'object' && value instanceof Blob;
  }

  /**
   * @param {string} model
   * @param {Object=} [filter]
   * @param {Callback} cb
   * @return {Promise.<Function>} listener destroyer
   * @public
   */
  listen (model, filter, cb) {
    if (typeof filter === 'function') {
      cb = filter;
      filter = null;
    }
    const s = new Subscription(this.socket, this, model, filter, cb);
    this.subscriptions.push(s);
    return s.request.then(() => s.id);
  }

  unlisten (id) {
    let index = this.subscriptions.findIndex(s => s.id === id);
    if (index !== -1) {
      this.subscriptions[index].destroy();
      this.subscriptions.splice(index, 1);
    }
  }

  /**
   * @private
   */
  updateSubscriptions () {
    this.subscriptions.forEach(S => S.update());
  }

  /**
   * @private
   */
  clearSubscriptions () {
    this.subscriptions.forEach(S => S.destroy());
  }
}
