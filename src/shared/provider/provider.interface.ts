export interface IProviderService {
  socket: any;
  list(a: any): any;
  me(): any;
  login(email: string, password: string): any;
  logout(): any;
  register (email: string, password: string, options: any);
  list(model: string, where: any): any;
  execute(model: string, command: any, data: any, method?: string);
  listen(model: string, filter: any, cb?: any);
}
