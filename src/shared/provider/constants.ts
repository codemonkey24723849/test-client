import { InjectionToken } from '@angular/core';
import { IProviderService } from './provider.interface';

export const PROVIDER_SERVICE = new InjectionToken<IProviderService>('app.provider-service');

