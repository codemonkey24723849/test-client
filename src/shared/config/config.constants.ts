import { InjectionToken } from '@angular/core';
import { IAppConfig } from './config.interface';

export const APP_CONFIG = new InjectionToken<IAppConfig>('app.config');
