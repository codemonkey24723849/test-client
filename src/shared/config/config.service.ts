import { Injectable, Inject } from '@angular/core';
import { APP_CONFIG } from './config.constants';
import { IAppConfig } from './config.interface';
import nestedProperty from 'nested-property';

@Injectable()
export class Config {
    constructor(@Inject(APP_CONFIG) private config: IAppConfig) {
    }

    get(value: string) {
        return nestedProperty.get(this.config, value);
    }

    has(value: string) {
        return nestedProperty.has(this.config, value);
    }

}
