import { NgModule } from '@angular/core';
import { Config } from './config.service';

@NgModule({
    providers: [
        Config
    ]
})
export class ConfigModule { }
