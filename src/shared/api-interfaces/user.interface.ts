export interface IApiUser {
    id?: number;
    email?: string;
    username?: string
    roles?: string[];
};
