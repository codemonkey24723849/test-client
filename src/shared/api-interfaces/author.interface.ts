export interface IApiAuthor {
  id?: number;
  firstName?: string;
  lastName?: string;
};
