import { IApiUser } from './user.interface';
import { IApiAuthor } from './author.interface';

export interface IApiBook {
  id?: number;
  name?: string;
  description?: string;
  smallThumbUrl?: string
  mediumThumbUrl?: string,
  author?: IApiAuthor,
  borrowedBy?: IApiUser
};
